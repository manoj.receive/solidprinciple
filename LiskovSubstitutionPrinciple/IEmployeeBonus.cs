﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples.LiskovSubstitutionPrinciple
{
    interface IEmployeeBonus
    {
        decimal CalculateBonus(decimal salary);
    }
}
