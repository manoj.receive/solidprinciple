﻿using SOLIDPrinciples.LiskovSubstitutionPrinciple;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples.OpenClosePrinciple
{
    #region Step 1 : Code for Open Close Principle 
    //Step 1 : Code for Open Close Principle 
    ////////////////// Below code is as per first requirements////////////////////////
    //public class Employee
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }

    //    public Employee()
    //    {

    //    }

    //    public Employee(int Id, string Name)
    //    {
    //        this.Id = Id; this.Name = Name;
    //    }

    //    public decimal CalculateBonus(decimal Salary) 
    //    {
    //        return Salary * .1M;
    //    }

    //    public override string ToString()
    //    {
    //        return string.Format("Id : {0} Name : {1}", this.Id, this.Name);
    //    }

    //}
    #endregion

    #region Step 2 : Code for Open Close Principle Updated Requirements
    //////////////// Step 2 Below code is as per first requirements////////////////////////
    //public abstract class Employee // Now open for extension and close for modification
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }

    //    public Employee()
    //    {

    //    }

    //    public Employee(int Id, string Name)
    //    {
    //        this.Id = Id; this.Name = Name;
    //    }

    //    // make this method as abstruct to adopt new changes
    //    public abstract decimal CalculateBonus(decimal Salary);


    //    public override string ToString()
    //    {
    //        return string.Format("Id : {0} Name : {1}", this.Id, this.Name);
    //    }

    //}

    ////Introducing a new class 

    //public class PermanentEmployee : Employee
    //{

    //    public PermanentEmployee()
    //    {

    //    }

    //    public PermanentEmployee(int Id, string Name) : base(Id, Name)
    //    {

    //    }
    //    public override decimal CalculateBonus(decimal Salary)
    //    {
    //        return Salary * .1M;
    //    }
    //}


    //public class TemproryEmployee : Employee
    //{

    //    public TemproryEmployee()
    //    {

    //    }

    //    public TemproryEmployee(int Id, string Name) : base(Id, Name)
    //    {

    //    }
    //    public override decimal CalculateBonus(decimal Salary)
    //    {
    //        return Salary * .05M;
    //    }
    //}

    #endregion

    #region Step 3 : Code for Liskov Substitution Principle
    public abstract class Employee : IEmployee, IEmployeeBonus
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Employee()
        {

        }

        public Employee(int Id, string Name)
        {
            this.Id = Id; this.Name = Name;
        }

        // make this method as abstruct to adopt new changes
        public abstract decimal CalculateBonus(decimal Salary);

        public abstract decimal GetMinimumSalary();
        
        public override string ToString()
        {
            return string.Format("Id : {0} Name : {1}", this.Id, this.Name);
        }

    }

    //Introducing a new class 

    public class PermanentEmployee : Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PermanentEmployee()
        {

        }

        public PermanentEmployee(int Id, string Name) : base(Id, Name)
        {

        }
        public override decimal CalculateBonus(decimal Salary)
        {
            return Salary * .1M;
        }

        public override decimal GetMinimumSalary()
        {
            return 15000;
        }
    }

    public class TemproryEmployee : Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TemproryEmployee()
        {

        }

        public TemproryEmployee(int Id, string Name) : base(Id, Name)
        {

        }
        public override decimal CalculateBonus(decimal Salary)
        {
            return Salary * .05M;
        }

        public override decimal GetMinimumSalary()
        {
            return 5000;
        }
    }

    public class ContractEmployee : IEmployee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ContractEmployee()
        {

        }

        public ContractEmployee(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }
       
        public decimal GetMinimumSalary()
        {
            return 5000;
        }
    }

    #endregion



}
