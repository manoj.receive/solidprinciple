﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples
{
    //Principle 1 : Single Responsiblity Principle.
    
    interface IUser
    {
        bool Login(string userName, string password);
        bool Register(string userName, string password, string email);
    }

    interface ILogger 
    {
        void logError(string error);
    }

    interface IEmail 
    {
        void sendEmail(string emailContent);
    }
}
