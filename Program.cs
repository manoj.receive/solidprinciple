﻿using SOLIDPrinciples.LiskovSubstitutionPrinciple;
using SOLIDPrinciples.OpenClosePrinciple;
using System;
using System.Collections.Generic;

namespace SOLIDPrinciples
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Code for Open Close Principle
            //Code for Open Close Principle
            // this will go obsolate as new modification are required -- Employee emp = new Employee(1, "John");
            // So as we added new classes to acomodate new changes for open close principle, will create object of new classes
            //Employee tmpEMp = new TemproryEmployee(1, "John");
            //Employee permEMp = new PermanentEmployee(1, "Jason");
            //Console.WriteLine(string.Format("Employee {0}, Bonus {1}", tmpEMp.ToString(), tmpEMp.CalculateBonus(100000).ToString()));
            //Console.WriteLine(string.Format("Employee {0}, Bonus {1}", permEMp.ToString(), permEMp.CalculateBonus(150000).ToString()));
            #endregion

            #region Liskov Substitution Principle
            List<Employee> employees = new List<Employee>();
            
            employees.Add(new PermanentEmployee(1, "John"));
            employees.Add(new TemproryEmployee(2, "Json"));
            //employees.Add(new ContractEmployee(3, "Mike"));
            foreach (var employee in employees)
            {
                Console.WriteLine(string.Format("Employee : {0}, Bonus {1}, Minimum Salary {2}", employee.Name, employee.CalculateBonus(10000).ToString(), employee.GetMinimumSalary().ToString()));
            }

            List<IEmployee> employeeslist = new List<IEmployee>();
            employeeslist.Add(new PermanentEmployee(1, "John"));
            employeeslist.Add(new TemproryEmployee(2, "Json"));
            employeeslist.Add(new ContractEmployee(3, "Mike"));
            foreach (var employee1 in employeeslist)
            {
                Console.WriteLine(string.Format("Employee : {0},  Minimum Salary {1}", employee1.Name, employee1.GetMinimumSalary().ToString()));
            }
            #endregion
            Console.ReadLine();
        }
    }
}
