﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples.DependencyInversionPrinciple.Interfaces
{
   public interface IRepositoryLayer
    {
        void save(Object details);
    }
}
