﻿using SOLIDPrinciples.DependencyInversionPrinciple.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples.DependencyInversionPrinciple
{

    public class BusinessLogcLayer
    {
        //private readonly DataAccessLayer DAL;
        private readonly IRepositoryLayer DAL;
        public BusinessLogcLayer(IRepositoryLayer repolayer)
        {
            DAL = repolayer;
        }

        public void Save(object details)
        {
            DAL.save(details);
        }
    }

    public class DataAccessLayer : IRepositoryLayer
    {
        public void save(object details)
        {
            // perform save
        }
       
    }
}
