﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples.ServiceClass
{
    //Principle 2 : Interface Segregation Principle
    public class HPLaseJet : IPrintTask
    {
        public bool PrintContent(string Content)
        {
            Console.WriteLine("Print Content");
            return true;
        }

        public bool ScanContent(string Content)
        {
            Console.WriteLine("Scan Content");
            return true;
        }

        public bool FaxContent(string Content)
        {
            Console.WriteLine("FaxContent");
            return true;
        }

        public bool PhotoCopyContent(string Content)
        {
            Console.WriteLine("Photo Copy Content");
            return true;
        }

        public bool DuplexPrintContent(string Content)
        {
            Console.WriteLine("DuplexPrintContent");
            return true;
        }
    }
}
