﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLIDPrinciples
{
    //Principle 2 : Interface Segregation Principle
    public interface IPrintTask
    {
        bool PrintContent(string Content);
        bool ScanContent(string Content);
        bool FaxContent(string Content);
        bool PhotoCopyContent(string Content);
        bool DuplexPrintContent(string Content);
    }

    // Step 2 : We can Segregate this one genural Interface into more focused multiple Interfaces like below.
    // So here we are segerating one single fat interface into multiple inter faces this is callled 
    // Interface Segregation Principle.

    interface IPrintCommon
    {
        bool PrintContent(string Content);
        bool ScanContent(string Content);
        bool PhotoCopyContent(string Content);
    }

    interface IFaxInterface
    {
        bool FaxContent(string Content);
    }

    interface IDuplexInterface
    {
        bool DuplexPrintContent(string Content);
    }
}
